package com.bermejo;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        User a = new User("Jose", "Rizal", "09991231231");

        User b = new User("Pedro", "Santos", "09994564564");

        // ArrayList
        ArrayList<User> usersList = new ArrayList<>();
        usersList.add(a);
        usersList.add(b);

        for (User user : usersList) {
           System.out.println(user.display());
        }

    }
}
